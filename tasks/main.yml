# @meta author: alexis_renard
# @meta author_linkedin: https://fr.linkedin.com/in/renardalexis
# @meta author_website: https://renardalexis.com
# @meta role_name: Set up Docker
# @meta description: Set up a new Docker environment on Ubuntu.
# @meta license: MIT
# @meta license_url: https://tldrlegal.com/license/mit-license
---
- name: Install aptitude using apt
  apt: name=aptitude state=latest update_cache=yes force_apt_get=yes
  tags:
    - apt #  : Using apt

- name: Install required system packages
  apt: name={{ item }} state=latest update_cache=yes
  loop: [ 'apt-transport-https', 'ca-certificates', 'curl', 'software-properties-common', 'python3-pip', 'virtualenv', 'python3-setuptools']
  tags:
    - apt

- name: Ensure old versions of Docker are not installed.
  package:
    name:
      - docker
      - docker-common
      - docker-engine
    state: absent
  tags:
    - packages

- name: Add Docker GPG apt Key
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    state: present
  tags:
    - apt_key

- name: Add Docker Repository
  apt_repository:
    repo: deb https://download.docker.com/linux/ubuntu bionic stable
    state: present
  tags:
    - apt_repository

- name: Update apt and install docker-ce
  apt: update_cache=yes name=docker-ce state=latest
  tags:
    - apt

- name: Install Docker Module for Python
  pip:
    name: docker
  tags:
    - pip
    - docker

- name: Import tasks to set up docker user and group.
  import_tasks: docker_user_and_group.yml

# Creates the number of containers defined by the variable create_containers, using values from vars file
- name: Pull default Docker image
  docker_image:
    name: "{{ default_container_image }}"
    pull: yes
  when: create_default_containers
  tags:
    - docker

- name: Run default containers
  docker_container:
    name: "{{ default_container_name }}"
    image: "{{ default_container_image }}"
    command: "{{ default_container_command }}"
    state: present
  when: create_default_containers
  tags:
    - docker
